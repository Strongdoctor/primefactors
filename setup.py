from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'Prime factorizer',
  ext_modules = cythonize("primefactors.pyx"),
)