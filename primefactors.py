from primefactors import prime_factors
import sys
try:
	i = int(sys.argv[1])
	print(prime_factors(i))
except:
	print("Invalid input!")